const Telegraf = require("telegraf");
const Markup = require("telegraf/markup");

const bot = new Telegraf(process.env.TELEGRAM_TOKEN);
const { Gitlab } = require("@gitbeaker/node");

bot.start((ctx) => {
  ctx.reply(
    `Hey, ${ctx.message.from.first_name}. This chat ID: ${ctx.chat.id}`
  );
});

bot.help((ctx) => {
  ctx.reply(
    `This bot manages your notanote data.

Send anything to this chat to make a new note. 
Write "@notanotemanagebot" when chatting with someone to get a list of recent notes.
Select any to send note's content. 
If you want to share your note via direct link instead (so that people could subscribe to note's updates), press "Create Link" button after sending a note.
`
  );
});
bot.command("search", (ctx) => {
  ctx.reply(
    `Hello ${ctx.message.from.first_name} ${ctx.message.from.last_name}, this is a TEST!`
  );
});
bot.on("sticker", (ctx) => {
  return ctx.reply("I cannot save stickers yet, sorry.");
});
bot.on("audio", (ctx) => {
  return ctx.reply("I cannot save audio yet, sorry.");
});
bot.on("contact", (ctx) => {
  return ctx.reply("I cannot save contacts yet, sorry.");
});
bot.on("document", (ctx) => {
  return ctx.reply("I cannot save documents yet, sorry.");
});
bot.on("voice", (ctx) => {
  return ctx.reply("I cannot save voices yet, sorry.");
});
bot.on("video_note", (ctx) => {
  return ctx.reply("I cannot save video notes yet, sorry.");
});
bot.on("inline_query", async ({ inlineQuery, answerInlineQuery }) => {
  const api = new Gitlab({
    token: "zU3E35X5dh1LLp6tQssG",
  });

  const project = await api.Projects.show(process.env.GITLAB_PROJECT_ID);
  const avatarUrl = project.avatarUrl;

  const files = await api.Repositories.tree(process.env.GITLAB_PROJECT_ID);

  // Replace file name with note's title
  for (
    let fileIndex = 0;
    fileIndex < files.length && fileIndex < 5;
    fileIndex++
  ) {
    const file = files[fileIndex];

    const fileRaw = await api.Repositories.showBlobRaw(
      process.env.GITLAB_PROJECT_ID,
      file.id
    );

    try {
      file.name = `"${JSON.parse(fileRaw.toString()).title}"`;
    } catch (e) {
      console.warn("Could not get a title for note", file);
    }
  }

  const results = files
    .filter(({ id }) => id)
    .map(({ name, path, id }) => ({
      type: "article",
      id: id,
      title: name,
      description: `Note "${path}"`,
      thumb_url: avatarUrl,
      input_message_content: {
        message_text: name,
      },
      reply_markup: Markup.inlineKeyboard([
        Markup.urlButton(
          "Open",
          `https://gitlab.com/api/v4/projects/${process.env.GITLAB_PROJECT_ID}/repository/blobs/${id}/raw`
        ),
      ]),
    }));
  return answerInlineQuery(results);
});

bot.on("chosen_inline_result", ({ chosenInlineResult }) => {
  console.log("chosen inline result", chosenInlineResult);
});

bot.on("location", (ctx) => {
  return ctx.reply("I cannot save location yet, sorry.");
});
bot.on("video", (ctx) => {
  return ctx.reply("I cannot save videos yet, sorry.");
});
bot.on("photo", (ctx) => {
  return ctx.reply("I cannot save photos yet, sorry.");
});

bot.on("text", async (ctx) => {
  if (!ctx.message) {
    return;
  }

  const api = new Gitlab({
    token: "zU3E35X5dh1LLp6tQssG",
  });

  let newNoteTitle = `${ctx.message.from}`;

  if (ctx.message.forward_from) {
    if (ctx.message.forward_from.username) {
      newNoteTitle = `${ctx.message.forward_from.username}`;
    } else {
      newNoteTitle = `${ctx.message.forward_from.first_name}`;
      if (ctx.message.forward_from.last_name) {
        newNoteTitle = `${newNoteTitle} ${ctx.message.forward_from.last_name}`;
      }
    }
    newNoteTitle = `${newNoteTitle}'s text message`;
  } else if (ctx.message.from) {
    if (ctx.message.from.username) {
      newNoteTitle = `${ctx.message.from.username}`;
    } else {
      newNoteTitle = `${ctx.message.from.first_name}`;
      if (ctx.message.from.last_name) {
        newNoteTitle = `${newNoteTitle} ${ctx.message.from.last_name}`;
      }
    }
    newNoteTitle = `${newNoteTitle}'s text message`;
  } else if (ctx.message.forward_from_chat) {
    newNoteTitle = `${ctx.message.forward_from_chat.type} chat text message`;
  }

  let newNoteTagString = `#NOTA_ID_${ctx.message.date}`;

  if (ctx.message.entities) {
    for (let index = 0; index < ctx.message.entities.length; index++) {
      const element = ctx.message.entities[index];
      newNoteTagString = `${newNoteTagString} #NOTA_${element.type.toUpperCase()}_${escape(
        ctx.message.text.substring(
          element.offset,
          element.offset + element.length
        )
      )}`;
    }
  }
  const result = await api.RepositoryFiles.create(
    process.env.GITLAB_PROJECT_ID,
    ctx.message.date,
    "master",
    `
    {
      "id":"${ctx.message.date}",
      "title":" ${newNoteTitle}",
      "content":
      {
        "time":1601887598705,
        "blocks": [{
          "type":"paragraph",
          "data": { 
            "text": ${JSON.stringify(ctx.message.text)}
          }},
          {
            "type":"paragraph",
            "data":
              {
                "text": "⸺ via Telegram ${newNoteTagString}"
              }
          }
        ],
        "version":"2.18.0"
      }
    }
    `,
    "Created a new note"
  );

  console.info("Created a new note on remote", result);

  await ctx.reply(`Created ${newNoteTitle} ${newNoteTagString}`);
});

const PORT = process.env.PORT;
const URL = process.env.BASE_URL + "webhook";
console.log("Registering webhook:" + URL);
bot.telegram.setWebhook(URL);
bot.startWebhook("/webhook", null, PORT);
